#!/bin/bash
set -e

echo "arguments: $#"
printf 'argument: [%q]\n' "$@"

env

exec "$@"
